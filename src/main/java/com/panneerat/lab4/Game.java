/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.panneerat.lab4;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class Game {

    private Player player1, player2;
    private Table table;
    private static boolean resetGame = true;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        printWelcome();
        while (resetGame) {
            newGame();
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (table.checkWin()) {
                    printTable();
                    printWinner();
                    printPlayers();
                    break;
                } else if (table.checkDraw()) {
                    printTable();
                    printDraw();
                    printPlayers();
                    break;
                }
                table.switchPlayer();
            }
            inputContinue();
        }
    }

    private void printWelcome() {
        System.out.println("Welcome to OX game");
    }

    private void printTable() {
        char[][] tb = table.getTable();
        System.out.println("--------------");
        for (int i = 0; i < 3; i++) {
            System.out.print(" | ");
            for (int j = 0; j < 3; j++) {
                System.out.print(tb[i][j] + " | ");
            }
            System.out.println();
            System.out.println("--------------");
        }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    //เช็คตำแหน่งซ้ำยังไม่ได้ทำ
    private void inputRowCol() {
        Scanner s = new Scanner(System.in);
        int row = 0;
        int col = 0;

        while (true) {
            System.out.print("Please input row col:  ");
            row = s.nextInt();
            col = s.nextInt();
            //เช็คตำแหน่ง input เข้ามาแล้วซ้ำ
            if (row >= 1 && row <= 3 && col >= 1 && col <= 3 && table.isEmpty(row, col)) {
                table.setRowCol(row, col);
                break;
            } else {
                System.out.println("Invalid move. Try again.");
            }
        }

    }

    private void printWinner() {
        System.out.println("Game Over ! Player " + table.getCurrentPlayer().getSymbol() + " win! ! !");
    }

    private void newGame() {
        table = new Table(player1, player2);

    }

    private void printDraw() {
        System.out.println("Game is Draw ! ! !");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void inputContinue() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please input continue (y/n)");
        String reset = s.next().trim().toLowerCase();
        resetGame = reset.equals("y");

    }
}
